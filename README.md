CONTENTS OF THIS FILE
---------------------
 * Introduction
 * Requirements
 * Configuration
 * Troubleshooting
 * FAQ
 * Maintainers

INTRODUCTION
------------
The Katch-to-YouTube program automates the process of transferring videos from a  
Katch repository (accessed by a weblink) to a YouTube account. This program  
allows users to input their account information for both Katch and YouTube and  
the Katch video link.


REQUIREMENTS
------------
This program uses the following modules:

 * <a href="http://splinter.readthedocs.org/en/latest/index.html" target="_blank">Splinter</a> 
 * <a href="https://developers.google.com/youtube/v3/code_samples/python#post_a_channel_bulletin"
    target="_blank">Youtube API</a> 
    * You will need two .JSON files for the program to work properly. The first  
    is named client_id.JSON and you should have received it when you activated  
    your google developers account. You can request another if you cannot find  
    it. It will also need a O2AUTH.JSON file. Run supplied script named  
    o2auth.py to obtain.
 * xvfbwrapper 

And the following programs:
 * Mozilla Firefox
 * XVFB
 

CONFIGURATION
-------------
For configuration, enter these commands to a linux terminal
>`$sudo pip install google-api-python-client splinter xvfbwrapper`<br />
>`$sudo apt-get install firefox xvfb` 

TROUBLESHOOTING
---------------
**Katch Credentials Error:**
If you encounter an error such as "Browser could not open" or "Could not  
download video", it is likely a problem with your Katch credentials.  
Double-check those and try again.

**Authentication Error:**
If you encounter an error such as "Could not upload video" or an HTTP error, it  
is likely the authentication .JSON files that are in error. To fix those please  
load in the proper files listed above.

FAQ
---
**To which YouTube account does the video upload?**  

It will upload to the account that is authenticated in the O2AUTH.JSON file.  
This will last as long as the file is there and the account is valid.  
  
**Do I need a Google Developers account?**  
  
There is a Google Developers account already attached to this script, so it is  
not necessary as long as the proper client_id.JSON file is there.  
  
**What is the Katch URL?**  
  
The Katch URL is the URL for the video you wish to transfer. Simply pull up the  
video on Katch and copy and paste the URL in your browser window into the proper  
field.

**What are keywords?**

Keywords are optional, but make your video more easily searchable (searching  
those terms can lead a user to your video). For more information, Google  
"Search Engine Optimization" (SEO).
  
**What do public & private mean?**
  
This is the privacy status of your uploaded video. If your video is _public_, it  
can be viewed in the same way as the YouTube videos you normally see. If your  
video is _private_, the only person who can see it on YouTube is you.  
  
**Why can't I find my video on YouTube?**  
  
One of several things could have happened. First, try searching for it by  
keywords and the title. If you still cannot find it, check the channel of the  
YouTube account. It can be hard to find if you made it private. If you still  
cannot find it, try uploading again. If the problem persists, please email a  
maintainer.  


MAINTAINERS
-----------
Current maintainers:
 * <a href="mailto:Andrew_Baas@baylor.edu">Andrew Baas</a>
