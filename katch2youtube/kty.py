import sys
import time
import httplib
import httplib2
import os
import random
import youtube_upload

from apiclient.discovery import build
from apiclient.errors import HttpError
from apiclient.http import MediaFileUpload
from oauth2client.client import flow_from_clientsecrets
from oauth2client.file import Storage
from oauth2client.tools import argparser, run_flow
from xvfbwrapper import Xvfb
from splinter import Browser

VALID_PRIVACY_STATUSES = ("public", "private", "unlisted")
TIMEOUT_TIME = 600

def parseArgs():
    "Parses the input arguments"
    argparser.add_argument("--user", required=True, help="Katch username")
    argparser.add_argument("--password", required=True, help="Katch password")
    argparser.add_argument("--url", required=True, help="URL ")
    argparser.add_argument("--folder", required=True, help="Folder to upload video to")
    argparser.add_argument("--file", help="File name to upload")
    argparser.add_argument("--title", help="Video title", default="Test Title")
    argparser.add_argument("--description", help="Video description",
        default="Test Description")
    argparser.add_argument("--category", default="22",
        help="Numeric video category. " +
        "See https://developers.google.com/youtube/v3/docs/videoCategories/list")
    argparser.add_argument("--keywords", help="Video keywords, comma separated",
        default="")
    argparser.add_argument("--privacyStatus", choices=VALID_PRIVACY_STATUSES,
        default=VALID_PRIVACY_STATUSES[0], help="Video privacy status.")
    return argparser.parse_args()

def initBrowser(folder):
    "Begins the browser with necessary profile preferences in Firefox"
    prof = {}
    prof['browser.download.manager.showWhenStarting'] = 'false'
    prof['browser.helperApps.alwaysAsk.force'] = 'false'
    prof['browser.download.dir'] = folder
    prof['browser.download.folderList'] = 2
    prof['browser.helperApps.neverAsk.saveToDisk'] = 'text/csv, application/csv, text/html,application/xhtml+xml,application/xml, application/octet-stream, application/pdf, application/x-msexcel,application/excel,application/x-excel,application/excel,application/x-excel,application/excel, application/vnd.ms- excel,application/x-excel,application/x-msexcel,image/png,image/jpeg,text/html,text/plain,application/msword,application/xml,application/excel,text/x-c'
    prof['browser.download.manager.useWindow'] = 'false'
    prof['browser.helperApps.useWindow'] = 'false'
    prof['browser.helperApps.showAlertonComplete'] = 'false'
    prof['browser.helperApps.alertOnEXEOpen'] = 'false'
    prof['browser.download.manager.focusWhenStarting']= 'false'
    return Browser('firefox',profile_preferences=prof)

def verifyLogin(username, password, browser):
    "Logs the user into the Twitter access of Katch"
    browser.visit('https://katch.me/user/signin')
    
    #Interacts with the webpage
    browser.fill("session[username_or_email]", username)
    browser.fill("session[password]", password)
    browser.find_by_id('allow').click()
    if browser.url != "https://katch.me/":
        raise ValueError('Login Failed', 'username: ' + username, 'password: ' + password)

def downloadVideo(webpage, browser, folder):
    "Downloads the video from the webpage"
    browser.visit(webpage) 
    time.sleep(4) 
    
    #Downloads file
    link = browser.find_link_by_partial_text('Download')
    browser.visit(link.first['href'])

    #Waits to kill the page till download is finished
    curTime = time.time()
    downloading = True
    while (downloading is True) and (time.time() < curTime + TIMEOUT_TIME):
        time.sleep(1)
        downloading = False
        for file in os.listdir(folder):
            if file.endswith(".part"):
                downloading = True    
                break

def renameVideo(folder):
    "Renames the downloaded video so that the next program can interact with it"
    for file in os.listdir(folder):
        if file.endswith(".ts"):
            os.rename(folder + "/" + file, folder + "/video.mpeg")
            break

def main():

    #XVFB wraps the browser so it can run headless
    xvfb = Xvfb()
    try:
        args = parseArgs()
        xvfb.start()
        print "Starting browser..."
        browser = initBrowser(args.folder)
        print "Browser started"
        print "Logging in..."
        verifyLogin(args.user, args.password, browser)
        print "Login successful"
        print "Downloading video..."
        downloadVideo(args.url, browser, args.folder)
        print "Video downloaded"
        xvfb.stop()
    except OSError:
        print(
            'Error: xvfb cannot be found on your system, please install '
            'it and try again')
        exit(1)
    except ValueError as error:
        print error.args
        exit(1)
    finally:
        browser.quit()
    print "Renaming video..."
    renameVideo(args.folder)
    print "Video renamed"
    args.file = args.folder + "/video.mpeg"
    youtube_upload.main(args) 

main()
