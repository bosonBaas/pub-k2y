import httplib
import httplib2
import os
import sys

from oauth2client.client import flow_from_clientsecrets
from oauth2client.file import Storage
from oauth2client.tools import argparser, run_flow

YOUTUBE_UPLOAD_SCOPE = "https://www.googleapis.com/auth/youtube.upload"
CLIENT_SECRETS_FILE = "client_id.json"
MISSING_CLIENT_SECRETS_MESSAGE = "not good"

args = argparser.parse_args()

flow = flow_from_clientsecrets(CLIENT_SECRETS_FILE,
    scope=YOUTUBE_UPLOAD_SCOPE,
    message=MISSING_CLIENT_SECRETS_MESSAGE)
storage = Storage("kty.py-oauth2.json")
credentials = storage.get()
credentials = run_flow(flow, storage, args)
