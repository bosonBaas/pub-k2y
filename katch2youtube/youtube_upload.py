import httplib
import httplib2
import os
import random
import sys
import time

from apiclient.discovery import build
from apiclient.errors import HttpError
from apiclient.http import MediaFileUpload
from oauth2client.client import flow_from_clientsecrets
from oauth2client.file import Storage
from oauth2client.tools import argparser, run_flow


# Explicitly tell the underlying HTTP transport library not to retry, since
# we are handling retry logic ourselves.
httplib2.RETRIES = 1

# Maximum number of times to retry before giving up.
MAX_RETRIES = 10

# Always retry when these exceptions are raised.
RETRIABLE_EXCEPTIONS = (httplib2.HttpLib2Error, IOError, httplib.NotConnected,
  httplib.IncompleteRead, httplib.ImproperConnectionState,
  httplib.CannotSendRequest, httplib.CannotSendHeader,
  httplib.ResponseNotReady, httplib.BadStatusLine)

# Always retry when an apiclient.errors.HttpError with one of these status
# codes is raised.
RETRIABLE_STATUS_CODES = [500, 502, 503, 504]

# this is the name of the file that contains the google
# developer's info. Place the file in the same location 
# as this script and put the name of the file here
# as long as the account is valid it will work
CLIENT_SECRETS_FILE = "client_id.json"

# the scope is the type of access you have. This scope allows
# the uploading of videos or files but nothing else
# the other two are constants and should not change
YOUTUBE_UPLOAD_SCOPE = "https://www.googleapis.com/auth/youtube.upload"
YOUTUBE_API_SERVICE_NAME = "youtube"
YOUTUBE_API_VERSION = "v3"

# This variable defines a message to display if the CLIENT_SECRETS_FILE is
# missing.
MISSING_CLIENT_SECRETS_MESSAGE = """
WARNING: Please configure OAuth 2.0

To make this sample run you will need to populate the client_secrets.json file

"""

# these are the valid values for the privacy status parameter
# if it is not one of these it will throw an error
VALID_PRIVACY_STATUSES = ("public", "private", "unlisted")

# This will load in the authentication file if it is there
# if not it will open a browser to request one
# and load that one if it is validated
def get_authenticated_service(args):
  flow = flow_from_clientsecrets(CLIENT_SECRETS_FILE,
    scope=YOUTUBE_UPLOAD_SCOPE,
    message=MISSING_CLIENT_SECRETS_MESSAGE)

  storage = Storage("%s-oauth2.json" % sys.argv[0])
  credentials = storage.get()

  if credentials is None or credentials.invalid:
    credentials = run_flow(flow, storage, args)

  return build(YOUTUBE_API_SERVICE_NAME, YOUTUBE_API_VERSION,
    http=credentials.authorize(httplib2.Http()))

# this is the function that begins the upload
def initialize_upload(youtube, options):
  tags = None
  # this properly parses the keywords and loads them
  if options.keywords:
    tags = options.keywords.split(",")
  # this formats the necessary variables for the 
  # video details like title category etc...
  body=dict(
    snippet=dict(
      title=options.title,
      description=options.description,
      tags=tags,
      categoryId=options.category
    ),
    status=dict(
      privacyStatus=options.privacyStatus
    )
  )

  # Call the API's videos.insert method to create and upload the video.
  insert_request = youtube.videos().insert(
    part=",".join(body.keys()),
    body=body,
    media_body=MediaFileUpload(options.file, chunksize=-1, resumable=True)
  ) # the chunksize varible defines how large the chunks of memory the
    # video will be uploaded in. -1 means one large chunk. This is 
    # standard but for bad connections 1024 or 2048 may be good numbers

  resumable_upload(insert_request)

# This method implements an exponential backoff strategy to resume a
# failed upload.
def resumable_upload(insert_request):
  response = None
  error = None
  retry = 0
  # keep trying until the video is uploaded or error is raised
  while response is None:
    try:
      # status update
      print "Uploading file..."
      # next chunk if necessary
      status, response = insert_request.next_chunk()
      if 'id' in response:
        # status complete update
        print "Video was successfully uploaded."
      else:
        # unkown error message
        exit("The upload failed with an unexpected response: %s" % response)
    except HttpError, e:
      # retriable error
      if e.resp.status in RETRIABLE_STATUS_CODES:
        error = "A retriable HTTP error %d occurred:\n%s" % (e.resp.status,
                                                             e.content)
      else:
        raise
    except RETRIABLE_EXCEPTIONS, e:
      # retriable error
      error = "A retriable error occurred: %s" % e
    # print error and retry if max retries is not reached
    if error is not None:
      print error
      retry += 1
      if retry > MAX_RETRIES:
        exit("No longer attempting to retry.")

      max_sleep = 2 ** retry
      sleep_seconds = random.random() * max_sleep
      # sleeping is user wants to quit
      print "Sleeping %f seconds and then retrying..." % sleep_seconds
      time.sleep(sleep_seconds)

def main(args):
  # make sure valid file
  if not os.path.exists(args.file):
    exit("Please specify a valid file using the --file= parameter.")
  # call function to obtain authentication
  youtube = get_authenticated_service(args)
  # begin uploading
  try:
    initialize_upload(youtube, args)
  # non retriable error
  except HttpError, e:
    print "An HTTP error %d occurred:\n%s" % (e.resp.status, e.content)
  # if completed remove file from server
  finally:
    os.remove(args.file)    

# collect command line arguments and pass to main
if __name__ == '__main__':
  argparser.add_argument("--file", required=True, help="Video file to upload")
  argparser.add_argument("--title", help="Video title", default="Test Title")
  argparser.add_argument("--description", help="Video description",
    default="Test Description")
  argparser.add_argument("--category", default="22",
    help="Numeric video category. " +
      "See https://developers.google.com/youtube/v3/docs/videoCategories/list")
  argparser.add_argument("--keywords", help="Video keywords, comma separated",
    default="")
  argparser.add_argument("--privacyStatus", choices=VALID_PRIVACY_STATUSES,
    default=VALID_PRIVACY_STATUSES[0], help="Video privacy status.")
  args = argparser.parse_args()
  main(args)
