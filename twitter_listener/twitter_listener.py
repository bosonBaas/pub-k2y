from twython import Twython
import time
# authentication info need to validate this outside of program
TWITTER_APP_KEY = "APP Key"
TWITTER_APP_KEY_SECRET = "Secret APP Key"
TWITTER_ACCESS_TOKEN = "your twitter access token"
TOKEN_SECRET = "your token secret"
# create validation and twython object
t = Twython(app_key=TWITTER_APP_KEY,
            app_secret=TWITTER_APP_KEY_SECRET,
            oauth_token=TWITTER_ACCESS_TOKEN,
            oauth_token_secret=TOKEN_SECRET)
# search variables concatonated with +
hashtag = "from:your_name_here+#katch"
#search 3 times and prints URL of found things
for i in range(0, 3):
    search = t.search(q=hashtag, count = 1, result_type="recent")
    tweets = search["statuses"]
    for tweet in tweets:
        print tweet["entities"]["urls"][0]["url"]
    time.sleep(5)

    
